Rails.application.routes.draw do

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  devise_for :users
  root 'static_pages#index'
  get 'unauthorized', to: 'static_pages#unauthorized'
  namespace :event_planner do 
    resources :events
  end
end
