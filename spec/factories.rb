FactoryBot.define do
  factory :user do
    sequence :email do |n|
      "Fakeemail#{n}@fake.com" 
    end
    password { "secretPassword" }
    password_confirmation { "secretPassword" }
    first_name { "fake_first_name" }
    last_name { "fake_last_name" }
  end

  factory :event do
    title { "test_title" } 
    venue { "test_venue_name" }
    address { "test_address" } 
    city { "test_city" }
    state { "test_state" }
    country_select { "test_country" }
    date { "2020-01-01" } 
    arrival_time { "2000-01-01 12:00:00" }
    description { "test_description" }
    association :user
  end
end