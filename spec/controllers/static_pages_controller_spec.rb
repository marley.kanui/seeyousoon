require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do

  describe "static_pages#index action" do

    it "should show the initial index page for the app" do 
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "static_pages#unauthorized action" do 

    it "should show the unauthorized page" do
      get :unauthorized 
      expect(response).to have_http_status(:success)
    end 
  end
end
