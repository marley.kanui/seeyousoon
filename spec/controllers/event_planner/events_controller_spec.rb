require 'rails_helper'

RSpec.describe EventPlanner::EventsController, type: :controller do

  describe "events#index action" do

    it "should successfully load the user event list page" do
      user = FactoryBot.create(:user)
      sign_in user
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "events#new action" do

    it "should successfully load the event create form" do 
      user = FactoryBot.create(:user)
      sign_in user
      get :new 
      expect(response).to have_http_status(:success)
    end
  end
end

  # describe "events#create" do

  #   it "it should succesffuly add an event to the database" do
  #     user = FactoryBot.create(:user)
  #     sign_in user
  #     post :create, params: { event: { title: 'test_title', venue: 'test_venue_name', address: 'test_address', city: 'test_city', state: 'test_state', country_select: 'test_country', date: '2020-01-01', arrival_time: '2000-01-01 12:00:00', description: 'test_description'} }
  #     expect(response).to redirect_to event_planner_events_path(user)

  #     event = Event.last
  #     expect(event.title).to eq("test_title")
  #     expect(event.venue).to eq("test_venue_name")
  #     expect(event.address).to eq("test_address")
  #     expect(event.city).to eq("test_city")
  #     expect(event.state).to eq("test_state")
  #     expect(event.country_select).to eq("test_country")
  #     # expect(event.date).to eq("Wed, 01 Jan 2020")
  #     expect(event.arrival_time).to eq("2000-01-01 12:00:00")
  #     expect(event.description).to eq("test_description")
  #     expect(event.user).to eq(user)
  #   end
  # end
# end 

  # describe "events#destroy action" do

  #   it "shouldn't allow users who didn't create the event to destroy the event" do 
  #     event = FactoryBot.create(:event)
  #     user = FactoryBot.create(:user)
  #     sign_in user
  #     delete :destroy, params: { id: event.id}
  #     expect(response).to have_http_status(:forbidden)
  #   end

    # it "shouldn't let unauthenticated users destroy an event" do
    #   event = FactoryBot.create(:event)
    #   delete :destroy, params: { id: event.id }
    #   expect(response).to redirect_to new_user_session_path
    # end

    # it "should allow a user to destroy events" do
    #   event = FactoryBot.create(:event)
    #   sign_in event.user
    #   delete :destroy, params: { id: event.id }
    #   expect(response).to redirect_to root_path 
    #   event = Event.find_by_id(event.id)
    #   expect(event).to eq nil
    # end

    # it "should return a 404 message if we cannot find an event with the id that is specified" do
    #   user = FactoryBot.create(:user)
    #   sign_in user
    #   delete :destroy, params: { id: 'invalid'}
    #   expect(response).to have_http_status(:not_found)
    # end
  # end

  # describe "events#update action" do

  #   it "shouldn't let user who did not create the event update the event" do 
  #     event = FactoryBot.create(:event)
  #     user = FactoryBot.create(:user)
  #     sign_in user
  #     patch :update, params: { id: event.id, event: { title: 'test_title', venue: 'test_venue_name', address: 'test_address', city: 'test_city', state: 'test_state', country_select: 'test_country', date: '2020-01-01', arrival_time: '2000-01-01 12:00:00', description: 'test_description'} }
  #     expect(response).to have_http_status(:forbidden)
  #   end 

  #   it "shouldn't let unauthenticated users update an event" do
  #     event = FactoryBot.create(:event)
  #     patch :update, params: { id: event.id, event: { title: 'test_title', venue: 'test_venue_name', address: 'test_address', city: 'test_city', state: 'test_state', country_select: 'test_country', date: '2020-01-01', arrival_time: '2000-01-01 12:00:00', description: 'test_description'} }
  #     expect(response).to redirect_to new_user_session_path
  #   end

  #   it "should allow users to successfully update events" do
  #     event = FactoryBot.create(:event, title: "initial_title", venue: "initial_venue_name", address: "initial_address", city: "initial_city", state: "initial_state", country_select: "initial_country", date: "2020-01-01", arrival_time: "2000-01-01 12:00:00", description: "initial_description")
  #     sign_in event.user

  #     patch :update, params: { id: event.id, event: { title: 'changed_title', venue: 'changed_venue_name', address: 'changed_address', city: 'changed_city', state: 'changed_state', country_select: 'changed_country', date: '2020-01-02', arrival_time: '2000-01-01 11:00:00', description: 'changed_description' } }
  #     expect(response).to redirect_to root_path
  #     event.reload
  #     expect(event.title).to eq "changed_title"
  #     expect(event.venue).to eq "changed_venue_name"
  #     expect(event.address).to eq "changed_address"
  #     expect(event.city).to eq "changed_city"
  #     expect(event.state).to eq "changed_state"
  #     expect(event.country_select).to eq "changed_country"
  #     # expect(event.date).to eq("Wed, 01 Jan 2020")
  #     expect(event.arrival_time).to eq "2000-01-01 11:00:00"
  #     expect(event.description).to eq "changed_description"
  #   end

  #   it "should have http 404 error if the event cannot be found" do
  #     user = FactoryBot.create(:user)
  #     sign_in user

  #     patch :update, params: { id: 'invalid_url', event: { title: 'changed_title', venue: 'changed_venue_name', address: 'changed_address', city: 'changed_city', state: 'changed_state', country_select: 'changed_country', date: '2020-01-02', arrival_time: '2000-01-01 11:00:00', description: 'changed_description' } }
  #     expect(response).to have_http_status(:not_found)
  #   end

  #   it "should render the edit form with an http status of unprocessable_entity" do
  #     event = FactoryBot.create(:event, title: "initial_title", venue: "initial_venue_name", address: "initial_address", city: "initial_city", state: "initial_state", country_select: "initial_country", date: "2020-01-01", arrival_time: "2000-01-01 12:00:00", description: "initial_description")
  #     sign_in event.user
  #     patch :update, params: { id: event.id, event: { title: '', venue: '', address: '', city: '', state: '', country_select: '', date: '', arrival_time: '', description: '' } }
  #     expect(response).to have_http_status(:unprocessable_entity)
  #     event.reload
  #     expect(event.title).to eq "initial_title"
  #     expect(event.venue).to eq "initial_venue_name"
  #     expect(event.address).to eq "initial_address"
  #     expect(event.city).to eq "initial_city"
  #     expect(event.state).to eq "initial_state"
  #     expect(event.country_select).to eq "initial_country"
  #     # expect(event.date).to eq("Wed, 01 Jan 2020")
  #     expect(event.arrival_time).to eq "2000-01-01 12:00:00"
  #     expect(event.description).to eq "initial_description"
  #   end
  # end

  # describe "events#edit action" do

  #   it "shouldn't let a user who did not create the event edit the event" do 
  #     event = FactoryBot.create(:event)
  #     user = FactoryBot.create(:user)
  #     sign_in user
  #     get :edit, params: { id: event.id }
  #     expect(response).to have_http_status(:forbidden)
  #   end

  #   it "shouldn't let unauthenticated users edit an event" do
  #     event = FactoryBot.create(:event)
  #     get :edit, params: { id: event.id }
  #     expect(response).to redirect_to new_user_session_path
  #   end   

  #   it "should successfully show the edit form if the event is found" do
  #     event = FactoryBot.create(:event)
  #     sign_in event.user

  #     get :edit, params: { id: event.id }
  #     expect(response).to have_http_status(:success)
  #   end

  #   it "should return a 404 error message if the event is not found" do
  #     user = FactoryBot.create(:user)
  #     sign_in user
  #     get :edit, params: { id: 'arbitrary_name' }
  #     expect(response).to have_http_status(:not_found)
  #   end
  # end

  # describe "events#show action" do

  #   it "should successfully show the page if the event is found" do
  #     user = FactoryBot.create(:user)
  #     sign_in user
  #     event = FactoryBot.create(:event)
  #     get :show, params: { id: event.id }
  #     expect(response).to have_http_status(:success)
  #   end

  #   # it "should redirect to event form if an event is not found" do
  #   #   user = FactoryBot.create(:user)
  #   #   sign_in user
  #   #   get :show, params: { id: event.last }
  #   #   expect(response).to redirect_to new_event_path
  #   # end
  #   # it "should successfully show the page if the event is found" do
  #   #   user = FactoryBot.create(:user)
  #   #   sign_in user
  #   #   event = user.events.create
  #   #   get :show, params: { id: event.id }
  #   #   expect(response).to have_http_status(:success)
  #   # end

  #   # it "should return a 404 error if the event is not found" do
  #   #   user = FactoryBot.create(:user)
  #   #   sign_in user
  #   #   get :show, params: { id: 'invalid_url' }
  #   #   expect(response).to have_http_status(:not_found)
  #   # end
  # end

  # describe "events#index action" do
  #   it "should successfully show the page" do 
  #     get :index
  #     expect(response).to have_http_status(:success)
  #   end
  # end

  # describe "events#new action" do 
  #   it "should require users to be logged in" do
  #     get :new
  #     expect(response).to redirect_to new_user_session_path
  #   end
  #   it "should successfully show the create event form" do 
  #     user = FactoryBot.create(:user)
  #     sign_in user

  #     get :new
  #     expect(response).to have_http_status(:success)
  #   end
  # end

  # describe "events#create action" do
  #   it "should successfully create a new event in the database" do
  #     user = FactoryBot.create(:user)
  #     sign_in user

  #     post :create, params: { event: { title: 'test_title', venue: 'test_venue_name', address: 'test_address', city: 'test_city', state: 'test_state', country_select: 'test_country', date: '2020-01-01', arrival_time: '2000-01-01 12:00:00', description: 'test_description'} }
  #     expect(response).to redirect_to event_path(user)

  #     event = Event.last
  #     expect(event.title).to eq("test_title")
  #     expect(event.venue).to eq("test_venue_name")
  #     expect(event.address).to eq("test_address")
  #     expect(event.city).to eq("test_city")
  #     expect(event.state).to eq("test_state")
  #     expect(event.country_select).to eq("test_country")
  #     # expect(event.date).to eq("Wed, 01 Jan 2020")
  #     expect(event.arrival_time).to eq("2000-01-01 12:00:00")
  #     expect(event.description).to eq("test_description")
  #     expect(event.user).to eq(user)
  #   end

  #   it "should properly deal with validation errors" do

  #     user = FactoryBot.create(:user)
  #     sign_in user

  #     post :create, params: { event: { title: '', venue: '', address: '', city: '', state: '', country_select: '', date: '', arrival_time: '', description: ''} }
  #     expect(response).to have_http_status(:unprocessable_entity)
  #     expect(Event.count).to eq Event.count
  #   end

  #   it "should require users to be logged in" do
  #     post :create, params: { event: { title: 'test_title', venue: 'test_venue_name', address: 'test_address', city: 'test_city', state: 'test_state', country_select: 'test_country', date: '2020-01-01', arrival_time: '2000-01-01 12:00:00', description: 'test_description'} }
  #     expect(response).to redirect_to new_user_session_path
  #   end
  # end
# end


