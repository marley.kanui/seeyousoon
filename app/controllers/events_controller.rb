class EventsController < ApplicationController
before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy, :show]

  def index
   
  end

  def show
    # @events = current_user.events

      # if @event.blank?
      #   render plain: 'Page not found', status: :not_found
      # end
    # if @events.blank?
    #   flash[:alert] = "Please Create an Event First"
    #   redirect_to new_event_path
    # elsif @events.user != current_user
    #   redirect_to unauthorized_path
    # end
    # return render_not_found(:forbidden) if @event.user != current_user
  end

  def edit 
    @event = Event.find_by_id(params[:id])
    return render_not_found if @event.blank?
    return render_not_found(:forbidden) if @event.user != current_user
  end 

  def update
    @event = Event.find_by_id(params[:id])
    return render_not_found if @event.blank?
    return render_not_found(:forbidden) if @event.user != current_user
    @event.update_attributes(event_params)

    if @event.valid?
      redirect_to root_path
    else
      return render :edit, status: :unprocessable_entity
    end
  end

  def destroy 
    @event = Event.find_by_id(params[:id])
    return render_not_found if @event.blank?
    return render_not_found(:forbidden) if @event.user != current_user
    @event.destroy
    redirect_to root_path
  end 

  def new
    @event = Event.new
  end

  def create
    @event = current_user.events.create(event_params)
    if @event.valid?
      redirect_to event_path(current_user)
    else
      render :new, status: :unprocessable_entity
    end
  end
   
  private 

  def event_params 
    params.require(:event).permit(:title, :venue, :address, :city, :state, :country_select, :date, :arrival_time, :description)
  end

  def render_not_found(status=:not_found)
    render plain: "Unauthorized Access", status: status
  end
end
