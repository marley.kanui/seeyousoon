class Event < ApplicationRecord
  validates :title, presence: true
  validates :venue, presence: true
  validates :address, presence: true
  validates :city, presence: true 
  validates :state, presence: true
  validates :country_select, presence: true
  validates :date, presence: true
  validates :arrival_time, presence: true
  validates :description, presence: true

  belongs_to :user
end
