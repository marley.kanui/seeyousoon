class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|

      t.string :title
      t.string :venue
      t.string :address
      t.string :city 
      t.string :state
      t.string :country_select
      t.date   :date
      t.time   :arrival_time
      t.text   :description

      t.timestamps
    end
  end
end
